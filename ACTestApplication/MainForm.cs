﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CSharpACLib;

namespace ACTestApplication
{
    public partial class MainForm : Form
    {
        private AccessControl _acc;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var config = new Configuration
            {
                BaseApiUrl = "https://acs.sergo-cheats.ru/",
                PublicKeyHash =
                    new byte[]
                    {
                        0xcb, 0xf9, 0x09, 0x73, 0x89,
                        0x00, 0xfa, 0xcd, 0xd8, 0xb0,
                        0x84, 0xc5, 0x28, 0xcd, 0x75,
                        0xfd, 0x2a, 0x31, 0x50, 0xfe
                    },
                HashSalt = "TestHash"
            };

            _acc = new AccessControl(config);

            ProductsCombobox.DataSource = _acc.GetProducts();

            ProductsCombobox.DisplayMember = "Name";
            ProductsCombobox.ValueMember = "Id";
        }

        private void SubmitBidButton_Click(object sender, EventArgs e)
        {
            try
            {
                var bid = _acc.RequestAccess((int)ProductsCombobox.SelectedValue);
                BidIdTextBox.Text = $"Номер заявки: {bid.Id}";
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Произошла ошибка при запросе доступа: {exception.Message}");
            }
        }
    }
}
